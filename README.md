## About

Port of the
[SRFI-158](https://srfi.schemers.org/srfi-158/srfi-158.html)
(Generators and Accumulators) reference implementation to CHICKEN.

## Docs

See [its wiki page].

[its wiki page]: https://wiki.call-cc.org/eggref/5/srfi-158
